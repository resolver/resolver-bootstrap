var BUILD_CONFIG = {
    lint: true,
    complexity: false,
    minify: false,
    debug: true
};

var fs = require("fs");
var gulp = require("gulp");
var gulpLoadPlugins = require("gulp-load-plugins");
var mainBowerFiles = require("main-bower-files");
var pkg = require("./package.json");
var _ = require("lodash");

var get_copyright = function () {
    "use strict";
    return fs.readFileSync('copyright_header.txt');
};

var plugins = gulpLoadPlugins();

var header_data = {
    name: pkg.name,
    version: pkg.version,
    author: pkg.author,
    homepage: pkg.homepage
};

gulp.task('styles', function () {
    "use strict";
    gulp.src("build.less")
        .pipe(plugins.less())
        .pipe(plugins.concat('resolver-bootstrap.css'))
        .pipe(plugins.header(get_copyright(), header_data))
        .pipe(gulp.dest("./dist/"));
});

gulp.task('build', ['styles']);

gulp.task('watch', function () {
    gulp.watch('themes/*.less', ['styles']);
});